#include <iostream>
#include <conio.h>
#include <string>
#include "Stack.h"
#include "Queue.h"

using namespace std;

int main()
{
	int size, choice, num;
	cout << "Enter size of element sets: ";
	cin >> size;
	system("cls");

	Stack<int> stack(size);
	Queue<int> queue(size);

	while (true)
	{
		cout << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything and empty set" << endl;
		cout << "4 - EXIT" << endl;
		cin >> choice;
		cout << "\n" << endl;
		if (choice == 1)
		{
			cout << "Enter number:  ";
			cin >> num;
			queue.push(num);
			stack.push(num);

			cout << "\nTop elements of set: " << endl;
			cout << "Queue: " << queue.top() << endl;
			cout << "Stack: " << stack.top() << endl;
			_getch();
			system("cls");
		}
		else if (choice == 2)
		{
			cout << "You have popped the front elements." << endl << endl;
			stack.pop();
			queue.pop();

			cout << "\nTop elements of set: " << endl;
			cout << "Queue: " << queue.top() << endl;
			cout << "Stack: " << stack.top() << endl;
			_getch();
			system("cls");
		}
		else if (choice == 3)
		{
			cout << "Queue elements: " << endl;
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << endl;
			}
			for (int i = 0; i < size; i++)
			{
				queue.pop();
			}

			cout << "Stack elements: " << endl;
			for (int i = 0; i < stack.getSize(); i++)
			{
				cout << stack[i] << endl;
			}
			for (int i = 0; i < size; i++)
			{
				stack.pop();
			}

			_getch();
			system("cls");
		}
		else if (choice == 4)
		{
			break;
		}
	}
}